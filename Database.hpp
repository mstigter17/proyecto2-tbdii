//
//  Database.hpp
//  ProyectoII_TBD2
//
//  Created by Mac on 2/26/18.
//  Copyright © 2018 Mac. All rights reserved.
//

#ifndef Database_hpp
#define Database_hpp

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include "Table.hpp"
#include "Columnas.hpp"
#include <vector>
#include "Archivo.hpp"
#include "MapaDeBits.hpp"
#include "Metadata.hpp"

using namespace std;

class Database{
public:
    string nombre;
    string terminacion;
    bool connected;
    int tamano;
    Database(Archivo * arch, string n,int t, string ter, int s);
    Database(string n, int t, Archivo * arch){
        nombre = n;
        tamano = t;
        archivo = arch;
    }
    ~Database();
    
    Archivo * archivo;
    MapaDeBits * mapaDeBits;
    Metadata * metadata;
    void generarMapaDeBits();
    int generarMetadata(int pos);
    
    Archivo * getArchivo();
    
    int cantBloques;
    int tamanoBloques;
    int calcularCantBloques();
    void setCantBloques();
    
    vector<Table*> tablesArray;
    int createTable(string n, vector<string> cols,vector<string> ti, string pk, vector<int> tamCol, int pos);
    void dropTable(string n);
    int insertDataToTable(string n, vector<string> data, int pos);
    void deleteDataFromTable(string n);
    
    void updateDataTable(string n,vector<string>valores);
    
    void selectTodo(string n);
    void selectAlgunos(string n, vector<string> vals);
    Table * getTable(string n);
    
};

#endif /* Database_hpp */
