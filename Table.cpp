//
//  Table.cpp
//  ProyectoII_TBD2
//
//  Created by Mac on 2/26/18.
//  Copyright © 2018 Mac. All rights reserved.
//

#include "Table.hpp"

Table::Table(string n){
    nombre = n;
}

void Table::addColumnas(vector<string> cols, vector<string> tipo, string pk){
    Columnas * nuevo;
    for(int a = 0; a<cols.size(); a++){
        if(tipo.at(a) == "int"){
            nuevo = new Columnas(cols.at(a),tipo.at(a),4);
        } else if(tipo.at(a) == "double"){
            nuevo = new Columnas(cols.at(a),tipo.at(a),8);
        } //FALTA CHAR
        if(cols.at(a) == pk){
            nuevo->isPrimaryKey();
        }
        columnas.push_back(nuevo);
    }
}

void Table::printCols(){
    cout<<"Tabla: "<<nombre<<endl;
    for(int a = 0; a<columnas.size(); a++){
        cout<<"Nombre: "<<columnas.at(a)->nombre<<endl;
        cout<<"Tipo: "<<columnas.at(a)->tipo<<endl;
        cout<<"Es Primary?: "<<columnas.at(a)->primaryKey<<endl;
    }
}

bool Table::checkIfColsExist(string nomsCols){
    for(int a = 0; a<columnas.size(); a++){
        if(columnas.at(a)->nombre == nomsCols){
            return true;
        }
    }
    return false;
}

Columnas * Table::getColumna(string n){
    for(int a = 0; a<columnas.size(); a++){
        if(columnas.at(a)->nombre == n){
            return columnas.at(a);
        }
    }
    return nullptr;
}

void Table::dropColumnas(){
    columnas.clear();
}

bool Table::checkSiColExiste(string n){
    for(int a = 0; a<columnas.size(); a++){
        if(columnas.at(a)->nombre == n){
            return true;
        }
    }
    return false;
}


