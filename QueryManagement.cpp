//
//  QueryManagement.cpp
//  ProyectoII_TBD2
//
//  Created by Mac on 3/2/18.
//  Copyright © 2018 Mac. All rights reserved.
//

#include "QueryManagement.hpp"



void QueryManagement::addToArray(Database * db){
    databaseArray.push_back(db);
}


void QueryManagement::dropDatabase(Database * db){
    int s = databaseArray.size();
    for(int a = 0; a<s; a++){
        if(databaseArray.at(a) == db){
            databaseArray.erase(databaseArray.begin()+a);
        }
    }
}

bool QueryManagement::checkIfDBExiste(string n){
    int s = databaseArray.size();
    for(int a = 0; a<s; a++){
        if(databaseArray.at(a)->nombre == n){
            return true;
        }
    }
    return false;
}

Database * QueryManagement::getDatabase(string n){
    int s = databaseArray.size();
    for(int a = 0; a<s; a++){
        if(databaseArray.at(a)->nombre == n){
            return databaseArray.at(a);
        }
    }
    return nullptr;
}

void QueryManagement::printVector(){
    int size = databaseArray.size();
    for(int a = 0; a<size; a++){
        cout<<databaseArray.at(a)->nombre<<" "<<databaseArray.at(a)->tamano<<endl;
    }
    cout<<"vector: "<<size<<endl;
}

void QueryManagement::validarQuery(string query){
    if(query.find("database") != string::npos ||
       query.find("connect") != string::npos){
        validarQueryDB(query);
    } else if(query.find("table") != string::npos ||
              query.find("insert") != string::npos ||
              query.find("delete") != string::npos ||
              query.find("update") != string::npos ||
              query.find("select") != string::npos){
        validarQueryTabla(query);
    } else{
        cout<<"QUERY NOT RECOGNIZED"<<endl;
    }
}

void QueryManagement::validarQueryDB(string query){
    if(query.find("create") != string::npos){
        string name;
        int lastPos;
        string s, termi;
        int size;
        for (int i=16; i<query.length(); i++){
            if(!isspace(query.at(i))){
                name+=query.at(i);
            } else{
                lastPos = i;
                break;
            }
        }
        for(int a = lastPos+1; a<query.length(); a++){
            if(!isspace(query.at(a))){
                s+=query.at(a);
            } else{
                lastPos = a;
                break;
            }
        }
        for(int a = lastPos+1; a<query.length(); a++){
            if(!ispunct(query.at(a))){
                termi+=query.at(a);
            } else{
                break;
            }
        }
        
        size = stoi(s);
        if(size < 128){
            char *cstr = new char[name.length() + 1];
            strcpy(cstr, name.c_str());
            Archivo * nuevoArch = new Archivo(cstr,size);
            nuevoArch->crear();
            Database * nuevo = new Database(nuevoArch,name,size,termi,4096);
            nameDB = name;
            sizeDB = size;
            nuevo->setCantBloques();
            nuevo->generarMapaDeBits();
            int posFinal = nuevo->generarMetadata(currentPos);
            currentPos+=posFinal;
            addToArray(nuevo);
            current = nuevo;
            cout<<"Database created!"<<endl;
            //printVector();
        } else{
            cout<<"Database size exceeded!"<<endl;
        }
        

    } else if(query.find("drop") != string::npos){
        string name;
        for (int i=14; i<query.length(); i++){
            if(!ispunct(query.at(i))){
                name+=query.at(i);
            } else{
                break;
            }
        }
        string end = ".txt";
        char * cstr2 = new char[end.length()];
        strcpy(cstr2, end.c_str());
        char * cstr = new char[end.length()];
        strcpy(cstr, name.c_str());
        remove(strcat(cstr,cstr2));
        cout<<"DATABASE DROPPED"<<endl;
    } else if(query.find("connect") != string::npos){
        string name;
        istringstream ss(query);
        string token;
        vector<string> querySplit;
        while(getline(ss, token, ' ')) {
            querySplit.push_back(token);
        }
        name = querySplit.at(2);
        
        ifstream f;
        string tamano;
        string line;
        string nombre;
        f.open(name+".txt",ios::out | ios::in );
        f.seekg(0);
        while(getline(f, line)) {
            if (line.find("NameDB:") != string::npos) {
                stringstream linestream(line);
                getline(linestream, nombre, ':');
                linestream >> nombre;
            } else if(line.find("TamañoDB:") != string::npos){
                stringstream linestream(line);
                getline(linestream, tamano, ':');
                linestream >> tamano;
            }
        }
        
        char * cstr = new char[name.length()+1];
        strcpy(cstr, name.c_str());
        Archivo * arch = new Archivo(cstr,stoi(tamano));
       Database * nue = new Database(name,stoi(tamano),arch);
        nameDB = name;
        sizeDB = stoi(tamano);
        current = nue;
        
        querySplit.clear();
    }
}

void QueryManagement::validarQueryTabla(string query){
    //current = temp; //PORMIENTRAS
    if(query.find("create") != string::npos){
        //split para ponerlos en vector
        istringstream ss(query);
        string token;
        string token2;
        string name;
        string pk;
        vector<string> nameCols;
        vector<string> tipoCols;
        vector<int> tamColsChar;
        vector<string> querySplit;
        while(getline(ss, token, ' ')) {
            querySplit.push_back(token);
        }
        name = querySplit.at(2);
        
        istringstream ss2(querySplit.at(3));
        vector<string> querySplit2;
        while(getline(ss2, token, '=')) {
            while(getline(ss2, token2, ',')){
                querySplit2.push_back(token2);
            }
        }
        
        for(int a = 0; a<querySplit2.size(); a++){
            nameCols.push_back(querySplit2.at(a));
        }
        
        istringstream ss3(querySplit.at(4));
        vector<string> querySplit3;
        while(getline(ss3, token, '=')) {
            while(getline(ss3, token2, ',')){
                querySplit3.push_back(token2);
            }
        }
        
        for(int a = 0; a<querySplit3.size(); a++){
            string token;
            string token3;
            if(querySplit3.at(a).find("char") != string::npos){
                tipoCols.push_back("char");
                istringstream ss3(querySplit3.at(a));
                while(getline(ss3, token, '(')) {
                    while(getline(ss3, token3, ')')){
                        tamColsChar.push_back(stoi(token3));
                    }
                }
            } else if(querySplit3.at(a).find("int") != string::npos){
                tipoCols.push_back(querySplit3.at(a));
                tamColsChar.push_back(4);
            } else if(querySplit3.at(a).find("double") != string::npos){
                tipoCols.push_back(querySplit3.at(a));
                tamColsChar.push_back(8);
            }
        }
        
        istringstream ss4(querySplit.at(5));
        vector<string> querySplit4;
        while(getline(ss4, token, '=')) {
            while(getline(ss4, token2, ';')){
                querySplit4.push_back(token2);
            }
        }
        
        pk = querySplit4.at(0);
        
        querySplit.clear();
        querySplit2.clear();
        querySplit3.clear();
        querySplit4.clear();
        
        

        int posFinal = current->createTable(name, nameCols, tipoCols, pk,tamColsChar,currentPos); //los esta adding al archivo
        currentPos+=posFinal;
        cout<<"TABLE CREATED!"<<endl;
       /* cout<<"current pos despues de suma: "<<currentPos<<endl;
        
        cout<<"NomTabla: "<<name<<endl;
        for(int a = 0;a<nameCols.size(); a++){
            cout<<"Name Col: "<<nameCols.at(a)<<endl;
        }
        for(int a = 0;a<nameCols.size(); a++){
            cout<<"Name tipo Col: "<<tipoCols.at(a)<<endl;
        }
        cout<<"PK: "<<pk<<endl;
        
        for(int a = 0; a<tamColsChar.size(); a++){
            cout<<"Tamaño de chars: "<<tamColsChar.at(a)<<endl;
        } */
        
        string str2 = ".txt";
        char * nom = new char[str2.length()+1];
        strcpy(nom, str2.c_str());
        
        char * nom2 = new char[current->nombre.length()+1];
        strcpy(nom2, current->nombre.c_str());
        strcat(nom2, nom);
        string input;
        
       /*ifstream inFile;
        inFile.seekg(0, inFile.end);
        int length = inFile.tellg();
       inFile.seekg (0, inFile.beg);
         inFile.open(nom2,ios::binary);
        inFile.seekg(0, fstream::beg);
        while ( getline (inFile,input) ){
            cout << input << '\n';
        }
       
        inFile.close(); */
        
        //cout<<current->archivo->leer(0, length, nom2)<<endl;
        
        /*
        Table * currTable = current->getTable(name);
        
        if(currTable != nullptr){
            currTable->addColumnas(nameCols, tipoCols, pk);
            currTable->printCols();
        } else{
            cout<<"es null"<<endl;
        }
        
        Archivo * a = current->getArchivo();
        current->guardarTablesToArchivo();
        currTable->guardarColumnasToArchivo(a);*/
        
        
       /*for(int a = 0; a<nameCols.size(); a++){
            if(nameCols.at(a).compare(pk) != 0){
                cout<<"Llave primaria, columna inexistente"<<endl;
            }
        }*/  //NO SIRVEE EL CHEQUEOO!!
    
        
       // }
        
    } else if(query.find("drop") != string::npos){
        string name;
        for (int i=11; i<query.length(); i++){
            if(!isspace(query.at(i))){
                name+=query.at(i);
            } else{
                break;
            }
        }
        current->dropTable(name);
          
    } else if(query.find("insert") != string::npos){
        vector<string> nameCols;
        vector<string> valores;
        string name = "";
        int lastPos = 0;
        string s,m;
        for (int i=7; i<query.length(); i++){ //get nom tabla
            if(!isspace(query.at(i))){
                name+=query.at(i);
            } else{
                lastPos = i;
                break;
            }
        }
        for(int a = lastPos+10; a<query.length(); a++){ //get nombre columnas
            if(!isspace(query.at(a))){
                if(!ispunct(query.at(a))){
                    s+=query.at(a);
                } else{
                    nameCols.push_back(s);
                    s = "";
                }
            } else{
                lastPos = a;
                break;
            }
        }
        
        for(int a = lastPos+9; a<query.length(); a++){ //get valores
            if(!isspace(query.at(a))){
                if(!ispunct(query.at(a))){
                    m+=query.at(a);
                } else{
                    valores.push_back(m);
                    m = "";
                }
            } else{
                break;
            }
        }
        
        int posFF = current->insertDataToTable(name, valores, currentPos);
        currentPos+=posFF;
        
    } else if(query.find("delete") != string::npos){
        string name;
        istringstream ss(query);
        string token;
        vector<string> querySplit;
        while(getline(ss, token, ' ')) {
            querySplit.push_back(token);
            std::cout << token << '\n';
        }
        name = querySplit.at(1);

        
      /*  istringstream ss2(querySplit.at(2));
        string token2;
        vector<string> querySplit2;
        while(getline(ss, token2, '=')) {
             while(getline(ss, token2, '=')) {
                 querySplit.push_back(token2);
                 std::cout << token2 << '\n';
             }
            
        }
        vector<string> constraints;
        //string name = "";
        int lastPos = 0;
        string s;
        for (int i=7; i<query.length(); i++){ //get nom tabla
            if(!isspace(query.at(i))){
                name+=query.at(i);
            } else{
                lastPos = i;
                break;
            }
        }
        
        for(int a = lastPos+8; a<query.length(); a++){
            if(!isspace(query.at(a))){
                if(!ispunct(query.at(a))){
                    s+=query.at(a);
                } else{
                    constraints.push_back(s);
                    s = "";
                }
            } else{
                break;
            }
        }
        
        cout<<"nom tabla: "<<name<<endl;
        cout<<"nom col: "<<constraints.at(0)<<endl;
        cout<<"nom val: "<<constraints.at(1)<<endl;
        */
        current->deleteDataFromTable(name);
        
    } else if(query.find("update") != string::npos){
        vector<string> nameCols;
        vector<string> constraints;
        vector<string> valores;
        string name = "";
        int lastPos = 0;
        string s,m,p;
        for (int i=7; i<query.length(); i++){ //get nom tabla
            if(!isspace(query.at(i))){
                name+=query.at(i);
            } else{
                lastPos = i;
                break;
            }
        }
        
        for(int a = lastPos+10; a<query.length(); a++){ //get nombre columnas
            if(!isspace(query.at(a))){
                if(!ispunct(query.at(a))){
                    s+=query.at(a);
                } else{
                    nameCols.push_back(s);
                    s = "";
                }
            } else{
                lastPos = a;
                break;
            }
        }
        
        for(int a = lastPos+9; a<query.length(); a++){ //get valores
            if(!isspace(query.at(a))){
                if(!ispunct(query.at(a))){
                    m+=query.at(a);
                } else{
                    valores.push_back(m);
                    m = "";
                }
            } else{
                lastPos = a;
                break;
            }
        }
        
        for(int a = lastPos+8; a<query.length(); a++){
            if(!isspace(query.at(a))){
                if(!ispunct(query.at(a))){
                    p+=query.at(a);
                } else{
                    constraints.push_back(p);
                    p = "";
                }
            } else{
                break;
            }
        } 
        
        current->updateDataTable(name,valores);
        
        cout<<"nom tabla: "<<name<<endl;
        cout<<"nom cols: "<<nameCols.at(0)<<endl;
        cout<<"valor: "<<valores.at(0)<<endl;
        cout<<"constraint: "<<constraints.at(0)<<endl;
        cout<<"constraint: "<<constraints.at(1)<<endl;
        
       
    } else if(query.find("select") != string::npos){
        istringstream ss(query);
        string token;
        string token2;
        string name;
        vector<string> seleccion;
        vector<string> nameCols;
         vector<string> constraints;
        vector<string> tipoCols;
        vector<int> tamColsChar;
        vector<string> querySplit;
        while(getline(ss, token, ' ')) {
            querySplit.push_back(token);
        }

        istringstream ss2(querySplit.at(2));
        vector<string> querySplit2;
        while(getline(ss2, token, '=')) {
            while(getline(ss2, token2, ';')) {
                querySplit2.push_back(token2);
            }
        }
        name = querySplit2.at(0);
        
        
        if(querySplit.at(1) == "*"){
            current->selectTodo(name);
        } else{
            istringstream ss3(querySplit.at(2));
            vector<string> querySplit3;
            while(getline(ss3, token, '=')) {
                while(getline(ss3, token2, ',')) {
                    querySplit3.push_back(token2);
                }
            }
            current->selectAlgunos(name,querySplit3);
        }

    }
        }
    //}
//}
