//
//  Columnas.hpp
//  ProyectoII_TBD2
//
//  Created by Mac on 2/26/18.
//  Copyright © 2018 Mac. All rights reserved.
//

#ifndef Columnas_hpp
#define Columnas_hpp

#include <stdio.h>
#include <iostream>
#include <vector>
#include <string>

using namespace std;

class Columnas {
public:
    struct Type{
        
    };
    
    string nombre;
    string tipo;
    bool primaryKey;
    
    Columnas(string n, string d, int t);
    
    void addData(string data);
    void printData();
    
    vector<int> datosInt;
    vector<double> datosDouble;
    vector<char*> datosChar;
    
    void isPrimaryKey();
    
    bool checkIfDataExists(string nameCol);
    void deleteData(string data);
    string getDataAt(string d);
    void updateData(string oldV, string newV);
    
    
};

#endif /* Columnas_hpp */
