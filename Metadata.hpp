//
//  Metadata.hpp
//  ProyectoII_TBD2
//
//  Created by Mac on 3/5/18.
//  Copyright © 2018 Mac. All rights reserved.
//

#ifndef Metadata_hpp
#define Metadata_hpp

#include <stdio.h>
#include "Archivo.hpp"
#include <fstream>

using namespace std;

class Metadata{
public:
    Metadata(Archivo * arch, char * nDB, int sDB, int cBl);
    Archivo * archivo;
    char * nameDB;
    int sizeDB;
    int cantBl;
    int cantTabs;
    
    int writeDBMetadata(int pos);
    void updateCantTablas(char * nDB);
    
    int setTableMetadata(char * nTB, int cCols, int cRegs, int nBl, int pos);
    void updateCantRegistros(char * nTB);
    void removeTableMetadata(char * nTB);
    int getNumBloqueTabla(char * nTB);
    int getCantRegsTabla(char * nTB);
    int getCantColsTabla(char * nTB);
    char * nameTB;
    int cantColumnas;
    int cantRegs;
    int numBloque;
    
    int setColMetadata(char * nCol, char * t, int sCol, bool k, int pos);
    char * nameCol;
    char * tipoCol;
    int sizeCol;
    bool key;
    
    
    
};

#endif /* Metadata_hpp */
