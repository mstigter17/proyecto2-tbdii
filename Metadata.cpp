//
//  Metadata.cpp
//  ProyectoII_TBD2
//
//  Created by Mac on 3/5/18.
//  Copyright © 2018 Mac. All rights reserved.
//

#include "Metadata.hpp"
#include <sstream>

Metadata::Metadata(Archivo * a,char * nDB, int sDB, int cBl){
    archivo = a;
    nameDB = nDB;
    sizeDB = sDB;
    cantBl = cBl;
    cantTabs = 0;
}

int Metadata::writeDBMetadata(int pos){
    
    string str2 = "NameDB:";
    char * nom = new char[str2.length()+1];
    strcpy(nom, str2.c_str());
    
    string str3 = "TamañoDB:";
    char * nom2 = new char[str3.length()+2];
    strcpy(nom2, str3.c_str());
    
    string str4 = "CantBloques:";
    char * nom3 = new char[str4.length()+1];
    strcpy(nom3, str4.c_str());
    
    string str5 = "CantTablas:";
    char * nom5 = new char[str5.length()+1];
    strcpy(nom5, str5.c_str());
    
    ofstream fout;
    fout.open(archivo->nombre,ios::out | ios::in);
    fout.seekp(pos);
    fout.write(strcat(nom, nameDB), 7+sizeof(nameDB));
    fout.write("\n",1);
    
    char * s = new char[to_string(sizeDB).length()+1];
    strcpy(s, to_string(sizeDB).c_str());
    fout.write(strcat(nom2,s),str3.length()+1); //not sure si 4 o length de char * memcpy???
    fout.write("\n",1);
    
    char * ca = new char[to_string(cantBl).length()+1];
    strcpy(ca, to_string(cantBl).c_str());
    fout.write(strcat(nom3,ca),str4.length()+4);
    fout.write("\n",1);
    
    string n1 = "0";
    char * c1 = new char[n1.length()];
    strcpy(c1, n1.c_str());
    fout.write(strcat(nom5,c1),str5.length()+1);
    fout.write("\n",1);
    
    
    
    int posFinal = fout.tellp();
    fout.close();
    
    delete [] nom;
    delete [] nom2;
    delete [] nom3;
    delete [] s;
    delete [] ca;
    
    return posFinal;
}

int Metadata::setTableMetadata(char *nTB, int cCols, int cRegs, int nBl, int pos){
    
    cantTabs++;
    string str2 = "Name:";
    char * nom = new char[str2.length()+1];
    strcpy(nom, str2.c_str());
    
    string cc = "CantColumnas:";
    char * ccch = new char[cc.length()+1];
    strcpy(ccch, cc.c_str());
    
    string cr = "CantReg:";
    char * crch = new char[cr.length()+1];
    strcpy(crch, cr.c_str());
    
    string cb = "NumBloque:";
    char * cbch = new char[cb.length()+1];
    strcpy(cbch, cb.c_str());
    
    ofstream fout;
    fout.open(archivo->nombre,ios::out | ios::in);
    fout.seekp(pos);
    
    fout.write(strcat(nom, nTB), 5+sizeof(nTB));
    fout.write("\n",1);
    
    char * cCo = new char[to_string(cCols).length()+1];
    strcpy(cCo, to_string(cCols).c_str());
    
    fout.write(strcat(ccch,cCo), cc.length()+to_string(cCols).length());
    fout.write("\n",1);
    
    char * creg = new char[to_string(cRegs).length()+1];
    strcpy(creg, to_string(cRegs).c_str());
    
    fout.write(strcat(crch,creg), cr.length()+1);
    fout.write("\n",1);
    
    char * cnbl = new char[to_string(nBl).length()+1];
    strcpy(cnbl, to_string(nBl).c_str());
    
    fout.write(strcat(cbch,cnbl), cb.length()+to_string(nBl).length());
    fout.write("\n",1);
    
    int posFinal = (int)fout.tellp();
    fout.close();
    
    delete [] nom;
    delete [] ccch;
    delete [] crch;
    delete [] cbch;
    delete [] cCo;
    delete [] creg;
    delete [] cnbl;
    
    return posFinal;
}

int Metadata::setColMetadata(char *nCol, char *t, int sCol, bool k, int pos){
    string str2 = "NameCol:";
    char * nom = new char[str2.length()+1];
    strcpy(nom, str2.c_str());
    
    string cc = "Tipo:";
    char * ccch = new char[cc.length()+1];
    strcpy(ccch, cc.c_str());
    
    string cr = "Tamano:";
    char * crch = new char[cr.length()+1];
    strcpy(crch, cr.c_str());
    
    string cb = "PK:";
    char * cbch = new char[cb.length()+1];
    strcpy(cbch, cb.c_str());
    
    ofstream fout;
    fout.open(archivo->nombre,ios::out | ios::in);
    fout.seekp(pos);
    fout.write(strcat(nom, nCol), 5+sizeof(nCol));
    fout.write("\n",1);
    
    
    fout.write(strcat(ccch,t), cc.length()+sizeof(t));
    fout.write("\n",1);
    
    char * creg = new char[to_string(sCol).length()+1];
    strcpy(creg, to_string(sCol).c_str());
    
    fout.write(strcat(crch,creg), cr.length()+to_string(sCol).length());
    fout.write("\n",1);
    
    if(k){
        string cb2 = "True";
        char * ctrue = new char[cb2.length()+1];
        strcpy(ctrue, cb2.c_str());
        fout.write(strcat(cbch,ctrue), cb.length()+cb2.length());
        fout.write("\n",1);
        delete [] ctrue;
    } else{
        string cb2 = "False";
        char * ctrue = new char[cb2.length()+1];
        strcpy(ctrue, cb2.c_str());
        fout.write(strcat(cbch,ctrue), cb.length()+cb2.length());
        fout.write("\n",1);
        delete [] ctrue;
    }

    int posFinal = fout.tellp();
    fout.close();
    
    delete [] nom;
    delete [] ccch;
    delete [] crch;
    delete [] cbch;
    delete [] creg;

    
    return posFinal;
}

void Metadata::updateCantRegistros(char * nTB){
    cantRegs++;
    ifstream f;
    string line;
    string nombre, numbl;
    f.open(archivo->nombre,ios::out | ios::in);
    f.seekg(0);
    while(getline(f, line)) {
        
        if (line.find("Name:") != string::npos) {
            stringstream   linestream(line);
            getline(linestream, nombre, ':');
            linestream >> nombre;
            char * cstr = new char[nombre.length()+1];
            strcpy(cstr, nombre.c_str());
            if(sizeof(cstr) == sizeof(nTB)){
                getline(f, line);
                int pos = f.tellg();
                stringstream   linestream(line);
                getline(linestream, nombre, ':');
                linestream >> numbl;
                f.close();
                ofstream fi;
                fi.open(archivo->nombre,ios::out | ios::in);
                fi.seekp(pos);
                string cr = "CantReg:";
                char * crch = new char[cr.length()+1];
                strcpy(crch, cr.c_str());
                
                char * creg = new char[to_string(cantRegs).length()+1];
                strcpy(creg, to_string(cantRegs).c_str());
                
                fi.write(strcat(crch,creg), cr.length()+1);
                fi.write("\n",1);
                delete [] creg;
                fi.close();
            }
        }
        
    }
}

void Metadata::updateCantTablas(char * nDB){
    ifstream f;
    string line;
    string nombre, numbl;
    f.open(archivo->nombre,ios::out | ios::in);
    f.seekg(0);
    while(getline(f, line)) {
        if (line.find("NameDB:") != string::npos) {
            stringstream   linestream(line);
            getline(linestream, nombre, ':');
            linestream >> nombre;
            char * cstr = new char[nombre.length()+1];
            strcpy(cstr, nombre.c_str());
            if(sizeof(cstr) == sizeof(nDB)){
                getline(f, line);
                getline(f, line);
                int pos = f.tellg();
                stringstream   linestream(line);
                getline(linestream, nombre, ':');
                linestream >> numbl;
                f.close();
                ofstream fi;
                fi.open(archivo->nombre,ios::out | ios::in);
                fi.seekp(pos);
                string cr = "CantTablas:";
                char * crch = new char[cr.length()+1];
                strcpy(crch, cr.c_str());
                
                char * creg = new char[to_string(cantTabs).length()+1];
                strcpy(creg, to_string(cantTabs).c_str());
                
                fi.write(strcat(crch,creg), cr.length()+1);

                 delete [] creg;
                fi.close();
            }
        }
        
    }
}

int Metadata::getNumBloqueTabla(char * nTB){
    ifstream f;
    string line;
    string nombre, numbl;
    string tt = ".txt";
    char * cstr = new char[tt.length()+1];
    strcpy(cstr, tt.c_str());
    f.open(archivo->nombre,ios::out | ios::in);
    f.seekg(0);
    while(getline(f, line)) {
        if (line.find("Name:") != string::npos) {
            stringstream   linestream(line);
            getline(linestream, nombre, ':');
            linestream >> nombre;
            
            char * cstr = new char[nombre.length()+1];
            strcpy(cstr, nombre.c_str());
            if(sizeof(cstr) == sizeof(nTB)){
                getline(f, line);
                getline(f, line);
                getline(f, line);
                stringstream   linestream2(line);
                getline(linestream2, nombre, ':');
                linestream2 >> numbl;
                f.close();
                return stoi(numbl);
            }
        }
        
    }
    return 0; //no existe
}

int Metadata::getCantRegsTabla(char * nTB){
    ifstream f;
    string line;
    string nombre, numbl;
    f.open(archivo->nombre,ios::out | ios::in);
    f.seekg(0);
    while(getline(f, line)) {
        if (line.find("Name:") != string::npos) {
            stringstream   linestream(line);
            getline(linestream, nombre, ':');
            linestream >> nombre;
            char * cstr = new char[nombre.length()+1];
            strcpy(cstr, nombre.c_str());
            if(sizeof(cstr) == sizeof(nTB)){
                getline(f, line);
                getline(f, line);
                stringstream   linestream(line);
                getline(linestream, nombre, ':');
                linestream >> numbl;
                f.close();
                return stoi(numbl);
            }
        }
    }
    return NULL; //no existe
}

int Metadata::getCantColsTabla(char * nTB){
    ifstream f;
    string line;
    string nombre, numbl;
    f.open(archivo->nombre,ios::out | ios::in);
    f.seekg(0);
    while(getline(f, line)) {
        if (line.find("Name:") != string::npos) {
            stringstream   linestream(line);
            getline(linestream, nombre, ':');
            linestream >> nombre;
            char * cstr = new char[nombre.length()+1];
            strcpy(cstr, nombre.c_str());
            if(sizeof(cstr) == sizeof(nTB)){
                getline(f, line);
                stringstream   linestream(line);
                getline(linestream, nombre, ':');
                linestream >> numbl;
                f.close();
                return stoi(numbl);
            }
        }
    }
    return NULL; //no existe
}

void Metadata::removeTableMetadata(char * nTB){
    ifstream f;
    string aQuitar;
    string line;
    string nombre, numbl;
    f.open(archivo->nombre,ios::out | ios::in);
    f.seekg(0);
    while(getline(f, line)) {
        if (line.find("Name:") != string::npos) {
            stringstream   linestream(line);
            getline(linestream, nombre, ':');
            linestream >> nombre;
            char * cstr = new char[nombre.length()+1];
            strcpy(cstr, nombre.c_str());
            if(sizeof(cstr) == sizeof(nTB)){
                int cantCols = getCantColsTabla(nTB);
                for(int a = 0; a<(cantCols*4)+4; a++){
                    getline(f, line);
                    line.replace(line.find(line), line.length(), "");
                }
                f.close();
            }
        }
        
    }
}

