//
//  Archivo.hpp
//  ProyectoII_TBD2
//
//  Created by Mac on 3/6/18.
//  Copyright © 2018 Mac. All rights reserved.
//

#ifndef Archivo_hpp
#define Archivo_hpp

#include <stdio.h>
#include <iostream>
#include <sstream>

using namespace std;

class Archivo{
public:
    Archivo(char * nombre, int size);
    void abrir();
    void crear();
    char * leer(int pos, int longitud);
    void escribir(char * data,int pos,int longitud);
    void cerrar();
    void setTamano(long pos);
    void formatear();
    FILE * file;
    char * nombre;
    int size;
};

#endif /* Archivo_hpp */
