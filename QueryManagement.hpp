//
//  QueryManagement.hpp
//  ProyectoII_TBD2
//
//  Created by Mac on 3/2/18.
//  Copyright © 2018 Mac. All rights reserved.
//

#ifndef QueryManagement_hpp
#define QueryManagement_hpp

#include <stdio.h>
#include <string>
#include <iostream>
#include <sstream>
#include "Database.hpp"
#include "Archivo.hpp"

class QueryManagement {
public:
    QueryManagement(){
        Archivo * tempA = new Archivo("Admin",1024);
        temp = new Database(tempA,"Admin",1024,"mb",4096);
        temp->calcularCantBloques();
        nameDB = "Admin";
        sizeDB = 1024;
        temp->generarMapaDeBits();
        temp->connected = false;
        currentPos = 0;
    };
    void validarQuery(string q);
    void validarQueryDB(string q);
    void validarQueryTabla(string q);
    void addToArray(Database * n);
    void dropDatabase(Database * n);
    bool checkIfDBExiste(string n);
    Database * getDatabase(string n);
    void printVector();
    
    string nameDB;
    int sizeDB;
    vector<Database*> databaseArray;
    Database * temp;
    Database * current;
    
    int currentPos;
    
    
};

#endif /* QueryManagement_hpp */
