//
//  MapaDeBits.hpp
//  ProyectoII_TBD2
//
//  Created by Mac on 3/5/18.
//  Copyright © 2018 Mac. All rights reserved.
//

#ifndef MapaDeBits_hpp
#define MapaDeBits_hpp

#include <stdio.h>
#include <iostream>
#include "Archivo.hpp"

using namespace std;

class MapaDeBits{
public:
    MapaDeBits(int cB);
    int cantBloques;
    int sigDisponible;
    
    void setSigDisponible(int s);
    int getSigDisponible();
    
    void generarMapaBits(Archivo * a);
};

#endif /* MapaDeBits_hpp */
