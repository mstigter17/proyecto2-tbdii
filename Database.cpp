//
//  Database.cpp
//  ProyectoII_TBD2
//
//  Created by Mac on 2/26/18.
//  Copyright © 2018 Mac. All rights reserved.
//

#include "Database.hpp"


Database::Database(Archivo * a, string n, int t, string ter, int s){
    archivo = a;
    nombre = n;
    connected = true;
    tamano = t;
    terminacion = ter;
    tamanoBloques = s;
}


Database::~Database(){

}

int Database::calcularCantBloques(){
    int enKBytes;
    int cb;
    if(terminacion == "mb"){
        enKBytes = tamano * 1024 * 1024;
        cb = enKBytes/4096;
        return cb;
    } else if(terminacion == "kb"){
        cb = tamano/4;
        return cb;
    } else if(terminacion == "gb"){
        enKBytes = tamano * 1000000;
        cb = enKBytes/4;
        return cb;
    }
    return 0;
}

Archivo * Database::getArchivo(){
    return archivo;
}

void Database::generarMapaDeBits(){
    mapaDeBits = new MapaDeBits(cantBloques);
}

int Database::generarMetadata(int pos){
    char * cstr = new char[nombre.length()+1];
    strcpy(cstr, nombre.c_str());
    metadata = new Metadata(archivo, cstr, tamano, cantBloques);
    int posf = metadata->writeDBMetadata(pos);
    delete [] cstr;
    return posf;
}

void Database::setCantBloques(){
    cantBloques = calcularCantBloques();
}

int Database::createTable(string n, vector<string> cols,vector<string> ti, string pk,vector<int> tamCol, int pos){

    char * cstr = new char[n.length()+1];
    strcpy(cstr, n.c_str());
    int pos2 = metadata->setTableMetadata(cstr, cols.size(), 0, mapaDeBits->getSigDisponible(),pos);
    mapaDeBits->setSigDisponible(mapaDeBits->getSigDisponible()+1);
    delete [] cstr;
    
    bool t;
    for(int a = 0; a<cols.size(); a++){
        t = false;
        for(int b = 0; b<cols.size(); b++){
            if(cols.at(b) == pk){
                t = true;
                break; //ARREGLAR EL PRIMARY KEY
            }
        }
        char * cstr = new char[cols.at(a).length()+1];
        strcpy(cstr, cols.at(a).c_str());
        char * cstr2 = new char[ti.at(a).length()+1];
        strcpy(cstr2, ti.at(a).c_str());
        
        int pos3 = metadata->setColMetadata(cstr, cstr2, tamCol.at(a), t, pos2);
        metadata->updateCantTablas(cstr);
        pos2+=pos3;
        t = false;
        
        delete [] cstr;
        delete [] cstr2;
    }
    
    return pos2;
    
}

Table * Database::getTable(string n){
    for(int a = 0; a<tablesArray.size(); a++){
        if(tablesArray.at(a)->nombre == n){
            return tablesArray.at(a);
        } 
    }
    return nullptr;
}

void Database::dropTable(string n){
    char * nom = new char[n.length()+1];
    strcpy(nom, n.c_str());
    int cant = metadata->getCantRegsTabla(nom);
    //no existe tb
        if(cant != 0){ //si hay registros
            int pos = metadata->getNumBloqueTabla(nom);
            ifstream fi;
            string line;
            fi.open(archivo->nombre,ios::out | ios::in);
            fi.seekg(pos*4096);
            for(int a = 0; a<cant; a++){
                getline(fi, line);
                line.replace(line.find("buenas,1,"), line.length(), " ");
            }
            fi.close();
        } //no hay registros pero se borra la metadata either way
    metadata->removeTableMetadata(nom);
        
}

int Database::insertDataToTable(string n, vector<string> data, int pos){
    char * nom = new char[n.length()+1];
    strcpy(nom, n.c_str());
    int cant = metadata->getNumBloqueTabla(nom);
    ofstream f;
    f.open(archivo->nombre,ios::out | ios::in);
    int poss = cant*4096 + pos;
    f.seekp(poss);
    for(int a = 0; a<data.size(); a++){
        char * dach = new char[data.at(a).length()+1];
        strcpy(dach, data.at(a).c_str());
        f.write(dach, data.at(a).length());
        f.write(",",1);
    }
    f.write("\n",1);
    metadata->updateCantRegistros(nom);
    int posFinal = f.tellp();
    f.close();
    return posFinal;
}

void Database::deleteDataFromTable(string n){
    string line;
    char * nom = new char[n.length()+1];
    strcpy(nom, n.c_str());
    string replace = " ";
    ifstream f;
    string tt = ".txt";
    char * cstr = new char[tt.length()+1];
    strcpy(cstr, tt.c_str());
    f.open(strcat(archivo->nombre,cstr),ios::out | ios::in);
    f.seekg(0);
    while(getline(f, line)) {
        if (line.find("Name:") != string::npos) {
            stringstream linestream(line);
            getline(linestream, nombre, ':');
            linestream >> nombre;
            char * cstr = new char[nombre.length()+1];
            strcpy(cstr, nombre.c_str());
            if(sizeof(cstr) == sizeof(nom)){
                int posData = metadata->getNumBloqueTabla(nom);
                int cantRegs = metadata->getCantRegsTabla(nom);
                int cantCols = metadata->getCantColsTabla(nom);
                int pos = posData*4096;
                f.seekg(pos);
                ofstream fo;
                fo.open(archivo->nombre,ios::out | ios::in);
                fo.seekp(pos);
                fo << replace;
            }
        }
    }
}

void Database::updateDataTable(string n,vector<string>valores){
    string line;
    char * nom = new char[n.length()+1];
    strcpy(nom, n.c_str());
    string replace = " ";
    ifstream f;
    f.open(archivo->nombre,ios::out | ios::in);
    f.seekg(0);
    while(getline(f, line)) {
        if (line.find("Name:") != string::npos) {
            stringstream linestream(line);
            getline(linestream, nombre, ':');
            linestream >> nombre;
            char * cstr = new char[nombre.length()+1];
            strcpy(cstr, nombre.c_str());
            if(sizeof(cstr) == sizeof(nom)){
                int posData = metadata->getNumBloqueTabla(nom);
                int cantRegs = metadata->getCantRegsTabla(nom);
                int cantCols = metadata->getCantColsTabla(nom);
                int pos = posData*4096;
                f.seekg(pos);
                for(int a = 0; a<valores.size(); a++){
                    getline(f, line);
                    ofstream fo;
                    fo.open(archivo->nombre,ios::out | ios::in);
                    fo.seekp(pos);
                    fo << replace;
                }
            }
        }
    }
}

void Database::selectTodo(string n){
    string line;
    char * nom = new char[n.length()+1];
    strcpy(nom, n.c_str());
    string replace = " ";
    ifstream f;
    f.open(archivo->nombre,ios::out | ios::in);
    f.seekg(0);
    while(getline(f, line)) {
        if (line.find("Name:") != string::npos) {
            stringstream linestream(line);
            getline(linestream, nombre, ':');
            linestream >> nombre;
            char * cstr = new char[nombre.length()+1];
            strcpy(cstr, nombre.c_str());
            if(sizeof(cstr) == sizeof(nom)){
                int posData = metadata->getNumBloqueTabla(nom);
                int cantRegs = metadata->getCantRegsTabla(nom);
                int cantCols = metadata->getCantColsTabla(nom);
                int pos = posData*4096;
                for(int a = 0; a<cantRegs; a++){
                    f.seekg(pos);
                    getline(f, line);
                    cout<<line<<endl;
                }
               
            }
        }
    }
}

void Database::selectAlgunos(string n, vector<string> vals){
    
}





