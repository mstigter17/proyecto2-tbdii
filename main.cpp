//
//  main.cpp
//  ProyectoII_TBD2
//
//  Created by Mac on 2/19/18.
//  Copyright © 2018 Mac. All rights reserved.
//

#include <iostream>
#include <string>
#include "QueryManagement.hpp"

using namespace std;

int main() {
  
   QueryManagement * queries = new QueryManagement();
    
    string query;
    while(query != "exit"){
        cout<<"dbcli>";
        getline(cin,query);
        queries->validarQuery(query);
    }
        
    
    queries->current->tablesArray.clear();
    queries->databaseArray.clear();
    delete queries->current;
    delete queries;
    
     return 0;
}
