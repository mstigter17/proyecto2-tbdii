//
//  Table.hpp
//  ProyectoII_TBD2
//
//  Created by Mac on 2/26/18.
//  Copyright © 2018 Mac. All rights reserved.
//

#ifndef Table_hpp
#define Table_hpp

#include <stdio.h>
#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include "Columnas.hpp"
#include "Archivo.hpp"

using namespace std;

class Table{
public:
    string nombre;
    
    vector<Columnas*> columnas;
    Table(string nombre);
    void addColumnas(vector<string> cols, vector<string> tipo, string pk);
    void printCols();
    bool checkIfColsExist(string nomsCols);
    bool checkSiColExiste(string n);
    Columnas * getColumna(string n);
    void dropColumnas();
    
    void guardarColumnasToArchivo(Archivo * arch);
    
};

#endif /* Table_hpp */
