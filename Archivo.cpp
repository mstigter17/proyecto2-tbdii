//
//  Archivo.cpp
//  ProyectoII_TBD2
//
//  Created by Mac on 3/6/18.
//  Copyright © 2018 Mac. All rights reserved.
//

#include "Archivo.hpp"
#include <fstream>

Archivo::Archivo(char * nombre,int s)
{
    this->nombre = nombre;
    size = s;
    
}

void Archivo::abrir(){
    file = fopen(nombre, "r+");
}

void Archivo::crear(){
    string str = ".txt";
    char *cstr = new char[str.length() + 1];
    strcpy(cstr, str.c_str());
    char * name = strcat(nombre, cstr);
    ofstream file;
    file.open(name, std::ofstream::in | std::ofstream::out | std::ofstream::app);
    file.close();
    
    ofstream myFile (name, ios::out | ios::in);
    myFile.seekp(size);
    myFile.write("\n",1);
    myFile.close();
}

void Archivo::formatear(){
    file = fopen(this->nombre,"w");
    fseek(file,4096*256,SEEK_SET);
    fputc('\0',file);
    fclose(file);
}


char * Archivo::leer(int pos, int longitud){
    ifstream fileR;
    fileR.open(this->nombre,ios::out | ios::in);
        fileR.seekg(pos);
        char * data = new char[longitud];
        fileR.read(data,longitud);
        fileR.close();
        return data;
}

void Archivo::escribir(char * data,int pos,int longitud){
    abrir();
    fseek(file, pos, SEEK_SET);
    fwrite(data, sizeof(char), longitud, file);
    cerrar();
}

void Archivo::setTamano(long pos){
    abrir();
    fseek(file, pos, SEEK_SET);
    fputc('\0', file);
    cerrar();
}


void Archivo::cerrar(){
    fclose(file);
}
