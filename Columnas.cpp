//
//  Columnas.cpp
//  ProyectoII_TBD2
//
//  Created by Mac on 2/26/18.
//  Copyright © 2018 Mac. All rights reserved.
//

#include "Columnas.hpp"

Columnas::Columnas(string n, string d, int t){
    nombre = n;
    tipo = d;
    primaryKey = false;
    
    if(tipo == "int"){
        datosInt.resize(4);
    } else if(tipo == "double"){
        datosDouble.resize(8);
    } else if(tipo == "char"){
        datosChar.resize(t);
    }
}

void Columnas::isPrimaryKey(){
    primaryKey = true;
}

void Columnas::addData(string data){
    if(tipo == "int"){
        datosInt.push_back(stoi(data));
    } else if(tipo == "double"){
        datosDouble.push_back(stod(data));
    } else if(tipo == "char"){
        char *cstr = new char[data.length() + 1];
        strcpy(cstr, data.c_str());
        datosChar.push_back(cstr);
        delete [] cstr;
    }
}

void Columnas::printData(){
    if(tipo == "int"){
        for(int a = 0; a<datosInt.size(); a++){
            cout<<nombre<<" "<<tipo<<" "<<datosInt.at(a)<<endl;
        }
    } else if(tipo == "double"){
        for(int a = 0; a<datosDouble.size(); a++){
            cout<<nombre<<" "<<datosDouble.at(a)<<endl;
        }
    } else if(tipo == "char"){
        for(int a = 0; a<datosChar.size(); a++){
            cout<<nombre<<" "<<datosChar.at(a)<<endl;
        }
    }
}

bool Columnas::checkIfDataExists(string data){
    if(tipo == "int"){
        for(int a = 0; a<datosInt.size(); a++){
            if(to_string(datosInt.at(a)) == data){
                return true;
            }
        }
    } else if(tipo == "double"){
        for(int a = 0; a<datosDouble.size(); a++){
            if(to_string(datosDouble.at(a)) == data){
                return true;
            }
        }
    } else if(tipo == "char"){
        for(int a = 0; a<datosChar.size(); a++){
            if(datosChar.at(a) == data){
                return true;
            }
        }
    }
    return false;
}

void Columnas::deleteData(string data){
    if(tipo == "int"){
        for(int a = 0; a<datosInt.size(); a++){
            if(to_string(datosInt.at(a)) == data){
                datosInt.erase(datosInt.begin()+a);
            }
        }
    } else if(tipo == "double"){
        for(int a = 0; a<datosDouble.size(); a++){
            if(to_string(datosDouble.at(a)) == data){
                datosDouble.erase(datosDouble.begin()+a);
            }
        }
    } else if(tipo == "char"){
        for(int a = 0; a<datosChar.size(); a++){
            if(datosChar.at(a) == data){
                datosChar.erase(datosChar.begin()+a);
            }
        }
    }
}

string Columnas::getDataAt(string data){
    if(tipo == "int"){
        for(int a = 0; a<datosInt.size(); a++){
            if(to_string(datosInt.at(a)) == data){
                return to_string(datosInt.at(a));
            }
        }
    } else if(tipo == "double"){
        for(int a = 0; a<datosDouble.size(); a++){
            if(to_string(datosDouble.at(a)) == data){
                return to_string(datosDouble.at(a));
            }
        }
    } else if(tipo == "char"){
        for(int a = 0; a<datosChar.size(); a++){
            if(datosChar.at(a) == data){
                string s(datosChar.at(a));
                return s;
            }
        }
    }
    return nullptr;
}

void Columnas::updateData(string oldV, string newV){
    if(tipo == "int"){
        replace(datosInt.begin(),datosInt.end(), stoi(oldV), stoi(newV));
    } else if(tipo == "double"){
        replace(datosDouble.begin(),datosDouble.end(), stod(oldV), stod(newV));
    } else if(tipo == "char"){
        char *cstr = new char[oldV.length() + 1];
        char *cstr2 = new char[newV.length() + 1];
        strcpy(cstr, oldV.c_str());
        strcpy(cstr2, newV.c_str());
        replace(datosChar.begin(),datosChar.end(), cstr, cstr2);
        delete [] cstr;
        delete [] cstr2;
    }
}

